sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/resource/ResourceModel",
	"sap/m/MessageToast"
], function (Controller,JSONModel,ResourceModel,MessageToast) {
	"use strict";

	return Controller.extend("Tutoriales.Walkthrough-08-translatables-text.controller.View1", {
		onInit: function () {
			var oData = {
				recipient: {
					name: "World"
				}
			};
			var oModel = new JSONModel(oData);
			this.getView().setModel(oModel);
			// set i18n model on view
			var i18nModel = new ResourceModel({
				bundleName: "Tutoriales.Walkthrough-08-translatables-text.i18n.i18n"
			});
			this.getView().setModel(i18nModel, "i18n");
		},
      onShowHello : function () {
         // read msg from i18n model
         var oBundle = this.getView().getModel("i18n").getResourceBundle();
         var sRecipient = this.getView().getModel().getProperty("/recipient/name");
         var sMsg = oBundle.getText("helloMsg", [sRecipient]);
         // show message
         MessageToast.show(sMsg);
      }
	});
});